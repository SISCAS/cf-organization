<?php
/**
 * Organization - Area
 *
 * @package Coordinator\Modules\Organization
 * @company Cogne Acciai Speciali s.p.a
 */

/**
 * Organization, Area class
 */
class cOrganizationArea extends cObject{

	/** Parameters */
	static protected $table="organization__areas";
	static protected $logs=true;

	/** Properties */
	protected $id;
	protected $deleted;
	protected $fkCompany;
	protected $name;
	protected $description;

	/**
	 * Check
	 *
	 * {@inheritdoc}
	 */
	protected function check(){
		// check properties
		if(!strlen(trim($this->fkCompany))){throw new Exception("Area, company key is mandatory..");}
		if(!strlen(trim($this->name))){throw new Exception("Area, name is mandatory..");}
		// return
		return true;
	}

	/**
	 * Get Label
	 *
	 * @return string Area label
	 */
	public function getLabel(){
		$label=$this->name;
		if($this->description){$label.=" (".$this->description.")";}
		return $label;
	}

	/**
	 * Get label with name and description in popup
	 *
	 * @return string
	 */
	public function getLabelPopup(){return api_link("#",$this->name,$this->description,"hidden-link",true);}

	/**
	 * Get Company
	 *
	 * @return cOrganizationCompany
	 */
	public function getCompany(){return new cOrganizationCompany($this->fkCompany);}

	/**
	 * Get Departments
	 *
	 * @param boolean $deleted Get also deleted entries
	 * @return object[]|false Array of entries objects or false
	 */
	public function getDepartments($deleted=false){return cOrganizationDepartment::availables($deleted,["fkArea"=>$this->id]);}

	/**
	 * Edit form
	 *
	 * @param string[] $additional_parameters Array of url additional parameters
	 * @return object Form structure
	 */
	public function form_edit(array $additional_parameters=array()){
		// build form
		$form=new strForm(api_url(array_merge(["mod"=>"organization","scr"=>"controller","act"=>"store","obj"=>"cOrganizationArea","idArea"=>$this->id],$additional_parameters)),"POST",null,null,"organization__area-edit_form");
		// fields
		$form->addField("select","fkCompany",api_text("cOrganizationArea-property-fkCompany"),$this->fkCompany,api_text("cOrganizationArea-placeholder-fkCompany"),null,null,null,"required");
		foreach(cOrganizationCompany::availables($this->exists()) as $company_fobj){$form->addFieldOption($company_fobj->id,$company_fobj->name);}
		$form->addField("text","name",api_text("cOrganizationArea-property-name"),$this->name,api_text("cOrganizationArea-placeholder-name"),null,null,null,"required");
		$form->addField("textarea","description",api_text("cOrganizationArea-property-description"),$this->description,api_text("cOrganizationArea-placeholder-description"),null,null,null,"rows='2'");
		// controls
		$form->addControl("submit",api_text("form-fc-save"));
		// return
		return $form;
	}

	/**
	 * Remove
	 *
	 * {@inheritdoc}
	 */
	public function remove(){
		if(!DEBUG){throw new Exception("Area remove function disabled by developer.. (enabled in debug mode)");}
		if(count($this->getDepartments(true))){throw new Exception("Area remove function disabled for already used areas");}
		else{parent::remove();}
	}

	// debug
	//protected function event_triggered($event){api_dump($event,static::class." event triggered");}

}
