<?php
/**
 * Organization - Division
 *
 * @package Coordinator\Modules\Organization
 * @company Cogne Acciai Speciali s.p.a
 */

/**
 * Organization, Division class
 */
class cOrganizationDivision extends cObject{

	/** Parameters */
	static protected $table="organization__divisions";
	static protected $logs=true;

	/** Properties */
	protected $id;
	protected $deleted;
	protected $fkCompany;
	protected $name;
	protected $description;

	/**
	 * Check
	 *
	 * {@inheritdoc}
	 */
	protected function check(){
		// check properties
		if(!strlen(trim($this->fkCompany))){throw new Exception("Division, company key is mandatory..");}
		if(!strlen(trim($this->name))){throw new Exception("Division, name is mandatory..");}
		// return
		return true;
	}

	/**
	 * Get Label
	 *
	 * @return string Division label
	 */
	public function getLabel(){
		$label=$this->name;
		if($this->description){$label.=" (".$this->description.")";}
		return $label;
	}

	/**
	 * Get Company
	 *
	 * @return cOrganizationCompany
	 */
	public function getCompany(){return new cOrganizationCompany($this->fkCompany);}

	/**
	 * Edit form
	 *
	 * @param string[] $additional_parameters Array of url additional parameters
	 * @return object Form structure
	 */
	public function form_edit(array $additional_parameters=array()){
		// build form
		$form=new strForm(api_url(array_merge(["mod"=>"organization","scr"=>"controller","act"=>"store","obj"=>"cOrganizationDivision","idDivision"=>$this->id],$additional_parameters)),"POST",null,null,"organization__division-edit_form");
		// fields
		$form->addField("select","fkCompany",api_text("cOrganizationDivision-property-fkCompany"),$this->fkCompany,api_text("cOrganizationDivision-placeholder-fkCompany"),null,null,null,"required");
		foreach(cOrganizationCompany::availables($this->exists()) as $company_fobj){$form->addFieldOption($company_fobj->id,$company_fobj->name);}
		$form->addField("text","name",api_text("cOrganizationDivision-property-name"),$this->name,api_text("cOrganizationDivision-placeholder-name"),null,null,null,"required");
		$form->addField("textarea","description",api_text("cOrganizationDivision-property-description"),$this->description,api_text("cOrganizationDivision-placeholder-description"),null,null,null,"rows='2'");
		// controls
		$form->addControl("submit",api_text("form-fc-save"));
		// return
		return $form;
	}

	/**
	 * Remove
	 *
	 * {@inheritdoc}
	 */
	public function remove(){
		if(!DEBUG){throw new Exception("Division remove function disabled by developer.. (enabled in debug mode)");}
		else{parent::remove();}
	}

	// debug
	//protected function event_triggered($event){api_dump($event,static::class." event triggered");}

}
