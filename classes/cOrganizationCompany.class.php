<?php
/**
 * Organization - Company
 *
 * @package Coordinator\Modules\Organization
 * @company Cogne Acciai Speciali s.p.a
 */

/**
 * Organization, Company class
 */
class cOrganizationCompany extends cObject{

	/** Parameters */
	static protected $table="organization__companies";
	static protected $logs=true;

	/** Properties */
	protected $id;
	protected $deleted;
	protected $name;
	protected $description;

	/**
	 * Check
	 *
	 * {@inheritdoc}
	 */
	protected function check(){
		// check properties
		if(!strlen(trim($this->name))){throw new Exception("Company, name is mandatory..");}
		// return
		return true;
	}

	/**
	 * Get Label
	 *
	 * @return string Company label
	 */
	public function getLabel(){
		$label=$this->name;
		if($this->description){$label.=" (".$this->description.")";}
		return $label;
	}

	/**
	 * Get Areas
	 *
	 * @param boolean $deleted Get also deleted entries
	 * @return object[]|false Array of entries objects or false
	 */
	public function getAreas($deleted=false){return cOrganizationArea::availables($deleted,["fkCompany"=>$this->id]);}

	/**
	 * Edit form
	 *
	 * @param string[] $additional_parameters Array of url additional parameters
	 * @return object Form structure
	 */
	public function form_edit(array $additional_parameters=array()){
		// build form
		$form=new strForm(api_url(array_merge(["mod"=>"organization","scr"=>"controller","act"=>"store","obj"=>"cOrganizationCompany","idCompany"=>$this->id],$additional_parameters)),"POST",null,null,"organization__company-edit_form");
		// fields
		$form->addField("text","name",api_text("cOrganizationCompany-property-name"),$this->name,api_text("cOrganizationCompany-placeholder-name"),null,null,null,"required");
		$form->addField("textarea","description",api_text("cOrganizationCompany-property-description"),$this->description,api_text("cOrganizationCompany-placeholder-description"),null,null,null,"rows='2'");
		// controls
		$form->addControl("submit",api_text("form-fc-save"));
		// return
		return $form;
	}

	/**
	 * Remove
	 *
	 * {@inheritdoc}
	 */
	public function remove(){
		if(!DEBUG){throw new Exception("Company remove function disabled by developer.. (enabled in debug mode)");}
		if(count($this->getAreas(true))){throw new Exception("Company remove function disabled for already used companies");}
		else{parent::remove();}
	}

	// debug
	//protected function event_triggered($event){api_dump($event,static::class." event triggered");}

}
