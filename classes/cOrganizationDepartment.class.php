<?php
/**
 * Organization - Department
 *
 * @package Coordinator\Modules\Organization
 * @area Cogne Acciai Speciali s.p.a
 */

/**
 * Organization, Department class
 */
class cOrganizationDepartment extends cObject{

	/** Parameters */
	static protected $table="organization__departments";
	static protected $logs=true;

	/** Properties */
	protected $id;
	protected $deleted;
	protected $fkArea;
	protected $name;
	protected $description;

	/**
	 * Check
	 *
	 * {@inheritdoc}
	 */
	protected function check(){
		// check properties
		if(!strlen(trim($this->fkArea))){throw new Exception("Department, area key is mandatory..");}
		if(!strlen(trim($this->name))){throw new Exception("Department, name is mandatory..");}
		// return
		return true;
	}

	/**
	 * Get Label
	 *
	 * @return string Department label
	 */
	public function getLabel(){
		$label=$this->name;
		if($this->description){$label.=" (".$this->description.")";}
		return $label;
	}

	/**
	 * Get label with name and description in popup
	 *
	 * @return string
	 */
	public function getLabelPopup(){return api_link("#",$this->name,$this->description,"hidden-link",true);}

	/**
	 * Get Area
	 *
	 * @return cOrganizationArea
	 */
	public function getArea(){return new cOrganizationArea($this->fkArea);}

	/**
	 * Edit form
	 *
	 * @param string[] $additional_parameters Array of url additional parameters
	 * @return object Form structure
	 */
	public function form_edit(array $additional_parameters=array()){
		// build form
		$form=new strForm(api_url(array_merge(["mod"=>"organization","scr"=>"controller","act"=>"store","obj"=>"cOrganizationDepartment","idDepartment"=>$this->id],$additional_parameters)),"POST",null,null,"organization__department-edit_form");
		// fields
		$form->addField("select","fkArea",api_text("cOrganizationDepartment-property-fkArea"),$this->fkArea,api_text("cOrganizationDepartment-placeholder-fkArea"),null,null,null,"required");
		foreach(cOrganizationArea::availables($this->exists()) as $area_fobj){$form->addFieldOption($area_fobj->id,$area_fobj->name);}
		$form->addField("text","name",api_text("cOrganizationDepartment-property-name"),$this->name,api_text("cOrganizationDepartment-placeholder-name"),null,null,null,"required");
		$form->addField("textarea","description",api_text("cOrganizationDepartment-property-description"),$this->description,api_text("cOrganizationDepartment-placeholder-description"),null,null,null,"rows='2'");
		// controls
		$form->addControl("submit",api_text("form-fc-save"));
		// return
		return $form;
	}

	/**
	 * Remove
	 *
	 * {@inheritdoc}
	 */
	public function remove(){
		if(!DEBUG){throw new Exception("Department remove function disabled by developer.. (enabled in debug mode)");}
		else{parent::remove();}
	}

	// debug
	//protected function event_triggered($event){api_dump($event,static::class." event triggered");}

}
