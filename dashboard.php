<?php
/**
 * Organization - Dashboard
 *
 * @package Coordinator\Modules\Organization
 * @company Cogne Acciai Speciali s.p.a
 */

// redirect to reports list
api_redirect(api_url(["scr"=>"management"]));