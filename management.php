<?php
/**
 * Organization - Management
 *
 * @package Coordinator\Modules\Organization
 * @company Cogne Acciai Speciali s.p.a
 *
 * @var strApplication $app
 */

// check authorizations
api_checkAuthorization("organization-manage","dashboard");
// include module template
require_once(MODULE_PATH."template.inc.php");
// set application title
$app->setTitle(api_text("management"));
// check for tab
if(!defined(TAB)){define("TAB","companies");}
// build navigation
$nav=new strNav("nav-pills");
$nav->addItem(api_text("management-nav-companies"),api_url(["scr"=>"management","tab"=>"companies"]));
$nav->addItem(api_text("management-nav-divisions"),api_url(["scr"=>"management","tab"=>"divisions"]));
$nav->addItem(api_text("management-nav-areas"),api_url(["scr"=>"management","tab"=>"areas"]));
$nav->addItem(api_text("management-nav-departments"),api_url(["scr"=>"management","tab"=>"departments"]));
// build grid
$grid=new strGrid();
$grid->addRow();
$grid->addCol($nav->render(false),"col-xs-12");
// switch tab
switch(TAB){
	/**
	 * Companies
	 *
	 * @var strTable $companies_table
	 * @var cOrganizationArea $selected_company_obj
	 */
	case "companies":
		// include tab
		require_once(MODULE_PATH."management-companies.inc.php");
		$grid->addRow();
		$grid->addCol($companies_table->render(),"col-xs-12");
		break;
	/**
	 * Divisions
	 *
	 * @var strTable $divisions_table
	 * @var cOrganizationDivision $selected_division_obj
	 */
	case "divisions":
		// include tab
		require_once(MODULE_PATH."management-divisions.inc.php");
		$grid->addRow();
		$grid->addCol($divisions_table->render(),"col-xs-12");
		break;
	/**
	 * Areas
	 *
	 * @var strTable $areas_table
	 * @var cOrganizationArea $selected_area_obj
	 */
	case "areas":
		// include tab
		require_once(MODULE_PATH."management-areas.inc.php");
		$grid->addRow();
		$grid->addCol($areas_table->render(),"col-xs-12");
		break;
	/**
	 * Departments
	 *
	 * @var strTable $departments_table
	 * @var cOrganizationDepartment $selected_department_obj
	 */
	case "departments":
		// include tab
		require_once(MODULE_PATH."management-departments.inc.php");
		$grid->addRow();
		$grid->addCol($departments_table->render(),"col-xs-12");
		break;
}
// add content to application
$app->addContent($grid->render());
// renderize application
$app->render();
// debug
if($selected_company_obj){api_dump($selected_company_obj,"selected company");}
if($selected_division_obj){api_dump($selected_division_obj,"selected division");}
if($selected_area_obj){api_dump($selected_area_obj,"selected area");}
if($selected_department_obj){api_dump($selected_department_obj,"selected department");}
