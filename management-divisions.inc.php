<?php
/**
 * Organization - Management (Divisions)
 *
 * @package Coordinator\Modules\Organization
 * @company Cogne Acciai Speciali s.p.a
 *
 * @var strApplication $app
 */

// build divisions table
$divisions_table=new strTable(api_text("management-divisions-tr-unvalued"));
$divisions_table->addHeader(api_text("cOrganizationDivision-property-fkCompany"),"nowrap");
$divisions_table->addHeader(api_text("cOrganizationDivision-property-name"),"nowrap");
$divisions_table->addHeader(api_text("cOrganizationDivision-property-description"),null,"100%");
$divisions_table->addHeaderAction(api_url(["scr"=>"management","tab"=>"divisions","act"=>"division_add"]),"fa-plus",api_text("table-td-add"),null,"text-right");

// cycle all divisions
foreach(cOrganizationDivision::availables(true) as $division_fobj){
	// build operation button
	$ob=new strOperationsButton();
	$ob->addElement(api_url(["scr"=>"management","tab"=>"divisions","act"=>"division_view","idDivision"=>$division_fobj->id]),"fa-info-circle",api_text("table-td-view"));
	$ob->addElement(api_url(["scr"=>"management","tab"=>"divisions","act"=>"division_edit","idDivision"=>$division_fobj->id]),"fa-pencil",api_text("table-td-edit"),(api_checkAuthorization("organization-manage")));
	if($division_fobj->deleted){$ob->addElement(api_url(["scr"=>"controller","act"=>"undelete","obj"=>"cOrganizationDivision","idDivision"=>$division_fobj->id,"return"=>["scr"=>"management","tab"=>"divisions"]]),"fa-trash-o",api_text("table-td-undelete"),true,api_text("cOrganizationDivision-confirm-undelete"));}
	else{$ob->addElement(api_url(["scr"=>"controller","act"=>"delete","obj"=>"cOrganizationDivision","idDivision"=>$division_fobj->id,"return"=>["scr"=>"management","tab"=>"divisions"]]),"fa-trash",api_text("table-td-delete"),true,api_text("cOrganizationDivision-confirm-delete"));}
	// make table row class
	$tr_class_array=array();
	if($division_fobj->id==$_REQUEST["idDivision"]){$tr_class_array[]="currentrow";}
	if($division_fobj->deleted){$tr_class_array[]="deleted";}
	// make Organization row
	$divisions_table->addRow(implode(" ",$tr_class_array));
	$divisions_table->addRowField($division_fobj->getCompany()->name,"nowrap");
	$divisions_table->addRowField($division_fobj->name,"nowrap");
	$divisions_table->addRowField($division_fobj->description,"truncate-ellipsis");
	$divisions_table->addRowField($ob->render(),"nowrap text-right");
}

// check for view action
if(ACTION=="division_view"){
	// get selected division
	$selected_division_obj=new cOrganizationDivision($_REQUEST["idDivision"]);
	// build description list
	$dl=new strDescriptionList("br","dl-horizontal");
	$dl->addElement(api_text("cOrganizationDivision-property-fkCompany"),$selected_division_obj->getCompany()->name);
	$dl->addElement(api_text("cOrganizationDivision-property-name"),api_tag("strong",$selected_division_obj->name));
	if($selected_division_obj->description){$dl->addElement(api_text("cOrganizationDivision-property-description"),$selected_division_obj->description);}
	// build modal
	$modal=new strModal(api_text("management-divisions-modal-title"),null,"management-divisions-view");
	$modal->setBody($dl->render()."<hr>".api_logs_table($selected_division_obj->getLogs((!$_REQUEST["all_logs"]?10:null)))->render());
	// add modal to application
	$app->addModal($modal);
	// modal scripts
	$app->addScript("$(function(){\$('#modal_management-divisions-view').modal();});");
}

// check for add or edit actions
if(in_array(ACTION,["division_add","division_edit"]) && api_checkAuthorization("organization-manage")){
	// get selected division
	$selected_division_obj=new cOrganizationDivision($_REQUEST["idDivision"]);
	// get form
	$form=$selected_division_obj->form_edit(["return"=>["scr"=>"management","tab"=>"divisions"]]);
	// additional controls
	$form->addControl("button",api_text("form-fc-cancel"),"#",null,null,null,"data-dismiss='modal'");
	if($selected_division_obj->exists()){$form->addControl("button",api_text("form-fc-remove"),api_url(["scr"=>"controller","act"=>"remove","obj"=>"cOrganizationDivision","idDivision"=>$selected_division_obj->id,"return"=>["scr"=>"management","tab"=>"divisions"]]),"btn-danger",api_text("cOrganizationDivision-confirm-remove"));}
	// build modal
	$modal=new strModal(api_text("management-divisions-modal-title"),null,"management-divisions-edit");
	$modal->setBody($form->render(1));
	// add modal to application
	$app->addModal($modal);
	// modal scripts
	$app->addScript("$(function(){\$('#modal_management-divisions-edit').modal({show:true,backdrop:'static',keyboard:false});});");
}
