<?php
/**
 * Organization - Functions
 *
 * @package Coordinator\Modules\Organization
 * @company Cogne Acciai Speciali s.p.a
 */

require_once(DIR."modules/organization/classes/cOrganizationCompany.class.php");
require_once(DIR."modules/organization/classes/cOrganizationDivision.class.php");
require_once(DIR."modules/organization/classes/cOrganizationArea.class.php");
require_once(DIR."modules/organization/classes/cOrganizationDepartment.class.php");
