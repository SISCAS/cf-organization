--
-- Organization - Setup (1.0.0)
--
-- @package Coordinator\Modules\Organization
-- @company Cogne Acciai Speciali s.p.a
--

-- --------------------------------------------------------

SET FOREIGN_KEY_CHECKS = 0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";

-- --------------------------------------------------------

--
-- Table structure for table `organization__companies`
--

CREATE TABLE IF NOT EXISTS `organization__companies` (
	`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
	`deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
	`name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
	`description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `organization__companies__logs`
--

CREATE TABLE IF NOT EXISTS `organization__companies__logs` (
	`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
	`fkObject` int(11) unsigned NOT NULL,
	`fkUser` int(11) unsigned DEFAULT NULL,
	`timestamp` int(11) unsigned NOT NULL,
	`alert` tinyint(1) unsigned NOT NULL DEFAULT '0',
	`event` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
	`properties_json` text COLLATE utf8_unicode_ci,
	PRIMARY KEY (`id`),
	KEY `fkObject` (`fkObject`),
	CONSTRAINT `organization__companies__logs_ibfk_1` FOREIGN KEY (`fkObject`) REFERENCES `organization__companies` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `organization__divisions`
--

CREATE TABLE IF NOT EXISTS `organization__divisions` (
	`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
	`deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
	`fkCompany` int(11) unsigned NOT NULL,
	`name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
	`description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
	PRIMARY KEY (`id`),
	KEY `fkCompany` (`fkCompany`),
	CONSTRAINT `organization__divisions_ibfk_1` FOREIGN KEY (`fkCompany`) REFERENCES `organization__companies` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `organization__divisions__logs`
--

CREATE TABLE IF NOT EXISTS `organization__divisions__logs` (
	`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
	`fkObject` int(11) unsigned NOT NULL,
	`fkUser` int(11) unsigned DEFAULT NULL,
	`timestamp` int(11) unsigned NOT NULL,
	`alert` tinyint(1) unsigned NOT NULL DEFAULT '0',
	`event` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
	`properties_json` text COLLATE utf8_unicode_ci,
	PRIMARY KEY (`id`),
	KEY `fkObject` (`fkObject`),
	CONSTRAINT `organization__divisions__logs_ibfk_1` FOREIGN KEY (`fkObject`) REFERENCES `organization__divisions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `organization__areas`
--

CREATE TABLE IF NOT EXISTS `organization__areas` (
	`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
	`deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
	`fkCompany` int(11) unsigned NOT NULL,
	`name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
	`description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
	PRIMARY KEY (`id`),
	KEY `fkCompany` (`fkCompany`),
	CONSTRAINT `organization__areas_ibfk_1` FOREIGN KEY (`fkCompany`) REFERENCES `organization__companies` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `organization__areas__logs`
--

CREATE TABLE IF NOT EXISTS `organization__areas__logs` (
	`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
	`fkObject` int(11) unsigned NOT NULL,
	`fkUser` int(11) unsigned DEFAULT NULL,
	`timestamp` int(11) unsigned NOT NULL,
	`alert` tinyint(1) unsigned NOT NULL DEFAULT '0',
	`event` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
	`properties_json` text COLLATE utf8_unicode_ci,
	PRIMARY KEY (`id`),
	KEY `fkObject` (`fkObject`),
	CONSTRAINT `organization__areas__logs_ibfk_1` FOREIGN KEY (`fkObject`) REFERENCES `organization__areas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `organization__departments`
--

CREATE TABLE IF NOT EXISTS `organization__departments` (
	`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
	`deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
	`fkArea` int(11) unsigned NOT NULL,
	`name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
	`description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
	PRIMARY KEY (`id`),
	KEY `fkArea` (`fkArea`),
	CONSTRAINT `organization__departments_ibfk_1` FOREIGN KEY (`fkArea`) REFERENCES `organization__areas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `organization__departments__logs`
--

CREATE TABLE IF NOT EXISTS `organization__departments__logs` (
	`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
	`fkObject` int(11) unsigned NOT NULL,
	`fkUser` int(11) unsigned DEFAULT NULL,
	`timestamp` int(11) unsigned NOT NULL,
	`alert` tinyint(1) unsigned NOT NULL DEFAULT '0',
	`event` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
	`properties_json` text COLLATE utf8_unicode_ci,
	PRIMARY KEY (`id`),
	KEY `fkObject` (`fkObject`),
	CONSTRAINT `organization__departments__logs_ibfk_1` FOREIGN KEY (`fkObject`) REFERENCES `organization__departments` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Authorizations
--

INSERT IGNORE INTO `framework__modules__authorizations` (`id`,`fkModule`,`order`) VALUES
('organization-manage','organization',1),
('organization-usage','organization',2);

-- --------------------------------------------------------

SET FOREIGN_KEY_CHECKS = 1;

-- --------------------------------------------------------
