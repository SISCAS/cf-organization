<?php
/**
 * Organization - Management (Companies)
 *
 * @package Coordinator\Modules\Organization
 * @company Cogne Acciai Speciali s.p.a
 *
 * @var strApplication $app
 */

// build companies table
$companies_table=new strTable(api_text("management-companies-tr-unvalued"));
$companies_table->addHeader(api_text("cOrganizationCompany-property-name"),"nowrap");
$companies_table->addHeader(api_text("cOrganizationCompany-property-description"),null,"100%");
$companies_table->addHeaderAction(api_url(["scr"=>"management","tab"=>"companies","act"=>"company_add"]),"fa-plus",api_text("table-td-add"),null,"text-right");

// cycle all companies
foreach(cOrganizationCompany::availables(true) as $company_fobj){
	// build operation button
	$ob=new strOperationsButton();
	$ob->addElement(api_url(["scr"=>"management","tab"=>"companies","act"=>"company_view","idCompany"=>$company_fobj->id]),"fa-info-circle",api_text("table-td-view"));
	$ob->addElement(api_url(["scr"=>"management","tab"=>"companies","act"=>"company_edit","idCompany"=>$company_fobj->id]),"fa-pencil",api_text("table-td-edit"),(api_checkAuthorization("organization-manage")));
	if($company_fobj->deleted){$ob->addElement(api_url(["scr"=>"controller","act"=>"undelete","obj"=>"cOrganizationCompany","idCompany"=>$company_fobj->id,"return"=>["scr"=>"management","tab"=>"companies"]]),"fa-trash-o",api_text("table-td-undelete"),true,api_text("cOrganizationCompany-confirm-undelete"));}
	else{$ob->addElement(api_url(["scr"=>"controller","act"=>"delete","obj"=>"cOrganizationCompany","idCompany"=>$company_fobj->id,"return"=>["scr"=>"management","tab"=>"companies"]]),"fa-trash",api_text("table-td-delete"),true,api_text("cOrganizationCompany-confirm-delete"));}
	// make table row class
	$tr_class_array=array();
	if($company_fobj->id==$_REQUEST["idCompany"]){$tr_class_array[]="currentrow";}
	if($company_fobj->deleted){$tr_class_array[]="deleted";}
	// make Organization row
	$companies_table->addRow(implode(" ",$tr_class_array));
	$companies_table->addRowField($company_fobj->name,"nowrap");
	$companies_table->addRowField($company_fobj->description,"truncate-ellipsis");
	$companies_table->addRowField($ob->render(),"nowrap text-right");
}

// check for view action
if(ACTION=="company_view"){
	// get selected company
	$selected_company_obj=new cOrganizationCompany($_REQUEST["idCompany"]);
	// build description list
	$dl=new strDescriptionList("br","dl-horizontal");
	$dl->addElement(api_text("cOrganizationCompany-property-name"),api_tag("strong",$selected_company_obj->name));
	if($selected_company_obj->description){$dl->addElement(api_text("cOrganizationCompany-property-description"),$selected_company_obj->description);}
	// build modal
	$modal=new strModal(api_text("management-companies-modal-title"),null,"management-companies-view");
	$modal->setBody($dl->render()."<hr>".api_logs_table($selected_company_obj->getLogs((!$_REQUEST["all_logs"]?10:null)))->render());
	// add modal to application
	$app->addModal($modal);
	// modal scripts
	$app->addScript("$(function(){\$('#modal_management-companies-view').modal();});");
}

// check for add or edit actions
if(in_array(ACTION,["company_add","company_edit"]) && api_checkAuthorization("organization-manage")){
	// get selected company
	$selected_company_obj=new cOrganizationCompany($_REQUEST["idCompany"]);
	// get form
	$form=$selected_company_obj->form_edit(["return"=>["scr"=>"management","tab"=>"companies"]]);
	// additional controls
	$form->addControl("button",api_text("form-fc-cancel"),"#",null,null,null,"data-dismiss='modal'");
	if($selected_company_obj->exists()){
		if($selected_company_obj->isDeleted()){
			$form->addControl("button",api_text("form-fc-undelete"),api_url(["scr"=>"controller","act"=>"undelete","obj"=>"cOrganizationCompany","idCompany"=>$selected_company_obj->id,"return"=>["scr"=>"management","tab"=>"companies"]]),"btn-warning",api_text("cOrganizationCompany-confirm-undelete"));
			$form->addControl("button",api_text("form-fc-remove"),api_url(["scr"=>"controller","act"=>"remove","obj"=>"cOrganizationCompany","idCompany"=>$selected_company_obj->id,"return"=>["scr"=>"management","tab"=>"companies"]]),"btn-danger",api_text("cOrganizationCompany-confirm-remove"));
		}else{$form->addControl("button",api_text("form-fc-delete"),api_url(["scr"=>"controller","act"=>"delete","obj"=>"cOrganizationCompany","idCompany"=>$selected_company_obj->id,"return"=>["scr"=>"management","tab"=>"companies"]]),"btn-danger",api_text("cOrganizationCompany-confirm-delete"));}
	}
	// build modal
	$modal=new strModal(api_text("management-companies-modal-title"),null,"management-companies-edit");
	$modal->setBody($form->render(1));
	// add modal to application
	$app->addModal($modal);
	// modal scripts
	$app->addScript("$(function(){\$('#modal_management-companies-edit').modal({show:true,backdrop:'static',keyboard:false});});");
}
