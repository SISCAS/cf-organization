<?php
/**
 * Organization - Template
 *
 * @package Coordinator\Modules\Organization
 * @company Cogne Acciai Speciali s.p.a
 */

// build application
$app=new strApplication();
// build nav
$nav=new strNav("nav-tabs");
// dashboard
$nav->addItem(api_icon("fa-th-large",null,"hidden-link"),api_url(["scr"=>"dashboard"]));
// management
if(api_checkAuthorization("organization-manage")){$nav->addItem(api_text("nav-management"),api_url(["scr"=>"management"]));}
// add nav to html
$app->addContent($nav->render(false));
