<?php
/**
 * Organization
 *
 * @package Coordinator\Modules\Organization
 * @company Cogne Acciai Speciali s.p.a
 * @authors Manuel Zavatta <manuel.zavatta@cogne.com>
 */
// definitions
$module_name="organization";
$module_repository_url="https://bitbucket.org/SISCAS/cf-organization/";
$module_repository_version_url="https://bitbucket.org/SISCAS/cf-organization/raw/master/VERSION.txt";
$module_required_modules=array();
