<?php
/**
 * Organization - Management (Departments)
 *
 * @package Coordinator\Modules\Organization
 * @area Cogne Acciai Speciali s.p.a
 *
 * @var strApplication $app
 */

// build departments table
$departments_table=new strTable(api_text("management-departments-tr-unvalued"));
$departments_table->addHeader(api_text("cOrganizationDepartment-property-fkArea"),"nowrap");
$departments_table->addHeader(api_text("cOrganizationDepartment-property-name"),"nowrap");
$departments_table->addHeader(api_text("cOrganizationDepartment-property-description"),null,"100%");
$departments_table->addHeaderAction(api_url(["scr"=>"management","tab"=>"departments","act"=>"department_add"]),"fa-plus",api_text("table-td-add"),null,"text-right");

// cycle all departments
foreach(cOrganizationDepartment::availables(true) as $department_fobj){
	// build operation button
	$ob=new strOperationsButton();
	$ob->addElement(api_url(["scr"=>"management","tab"=>"departments","act"=>"department_view","idDepartment"=>$department_fobj->id]),"fa-info-circle",api_text("table-td-view"));
	$ob->addElement(api_url(["scr"=>"management","tab"=>"departments","act"=>"department_edit","idDepartment"=>$department_fobj->id]),"fa-pencil",api_text("table-td-edit"),(api_checkAuthorization("organization-manage")));
	if($department_fobj->deleted){$ob->addElement(api_url(["scr"=>"controller","act"=>"undelete","obj"=>"cOrganizationDepartment","idDepartment"=>$department_fobj->id,"return"=>["scr"=>"management","tab"=>"departments"]]),"fa-trash-o",api_text("table-td-undelete"),true,api_text("cOrganizationDepartment-confirm-undelete"));}
	else{$ob->addElement(api_url(["scr"=>"controller","act"=>"delete","obj"=>"cOrganizationDepartment","idDepartment"=>$department_fobj->id,"return"=>["scr"=>"management","tab"=>"departments"]]),"fa-trash",api_text("table-td-delete"),true,api_text("cOrganizationDepartment-confirm-delete"));}
	// make table row class
	$tr_class_array=array();
	if($department_fobj->id==$_REQUEST["idDepartment"]){$tr_class_array[]="currentrow";}
	if($department_fobj->deleted){$tr_class_array[]="deleted";}
	// make Organization row
	$departments_table->addRow(implode(" ",$tr_class_array));
	$departments_table->addRowField($department_fobj->getArea()->name,"nowrap");
	$departments_table->addRowField($department_fobj->name,"nowrap");
	$departments_table->addRowField($department_fobj->description,"truncate-ellipsis");
	$departments_table->addRowField($ob->render(),"nowrap text-right");
}

// check for view action
if(ACTION=="department_view"){
	// get selected department
	$selected_department_obj=new cOrganizationDepartment($_REQUEST["idDepartment"]);
	// build description list
	$dl=new strDescriptionList("br","dl-horizontal");
	$dl->addElement(api_text("cOrganizationDepartment-property-fkArea"),$selected_department_obj->getArea()->name);
	$dl->addElement(api_text("cOrganizationDepartment-property-name"),api_tag("strong",$selected_department_obj->name));
	if($selected_department_obj->description){$dl->addElement(api_text("cOrganizationDepartment-property-description"),$selected_department_obj->description);}
	// build modal
	$modal=new strModal(api_text("management-departments-modal-title"),null,"management-departments-view");
	$modal->setBody($dl->render()."<hr>".api_logs_table($selected_department_obj->getLogs((!$_REQUEST["all_logs"]?10:null)))->render());
	// add modal to application
	$app->addModal($modal);
	// modal scripts
	$app->addScript("$(function(){\$('#modal_management-departments-view').modal();});");
}

// check for add or edit actions
if(in_array(ACTION,["department_add","department_edit"]) && api_checkAuthorization("organization-manage")){
	// get selected department
	$selected_department_obj=new cOrganizationDepartment($_REQUEST["idDepartment"]);
	// get form
	$form=$selected_department_obj->form_edit(["return"=>["scr"=>"management","tab"=>"departments"]]);
	// additional controls
	$form->addControl("button",api_text("form-fc-cancel"),"#",null,null,null,"data-dismiss='modal'");
	if($selected_department_obj->exists()){$form->addControl("button",api_text("form-fc-remove"),api_url(["scr"=>"controller","act"=>"remove","obj"=>"cOrganizationDepartment","idDepartment"=>$selected_department_obj->id,"return"=>["scr"=>"management","tab"=>"departments"]]),"btn-danger",api_text("cOrganizationDepartment-confirm-remove"));}
	// build modal
	$modal=new strModal(api_text("management-departments-modal-title"),null,"management-departments-edit");
	$modal->setBody($form->render(1));
	// add modal to application
	$app->addModal($modal);
	// modal scripts
	$app->addScript("$(function(){\$('#modal_management-departments-edit').modal({show:true,backdrop:'static',keyboard:false});});");
}
