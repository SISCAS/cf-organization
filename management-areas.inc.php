<?php
/**
 * Organization - Management (Areas)
 *
 * @package Coordinator\Modules\Organization
 * @company Cogne Acciai Speciali s.p.a
 *
 * @var strApplication $app
 */

// build areas table
$areas_table=new strTable(api_text("management-areas-tr-unvalued"));
$areas_table->addHeader(api_text("cOrganizationArea-property-fkCompany"),"nowrap");
$areas_table->addHeader(api_text("cOrganizationArea-property-name"),"nowrap");
$areas_table->addHeader(api_text("cOrganizationArea-property-description"),null,"100%");
$areas_table->addHeaderAction(api_url(["scr"=>"management","tab"=>"areas","act"=>"area_add"]),"fa-plus",api_text("table-td-add"),null,"text-right");

// cycle all areas
foreach(cOrganizationArea::availables(true) as $area_fobj){
	// build operation button
	$ob=new strOperationsButton();
	$ob->addElement(api_url(["scr"=>"management","tab"=>"areas","act"=>"area_view","idArea"=>$area_fobj->id]),"fa-info-circle",api_text("table-td-view"));
	$ob->addElement(api_url(["scr"=>"management","tab"=>"areas","act"=>"area_edit","idArea"=>$area_fobj->id]),"fa-pencil",api_text("table-td-edit"),(api_checkAuthorization("organization-manage")));
	if($area_fobj->deleted){$ob->addElement(api_url(["scr"=>"controller","act"=>"undelete","obj"=>"cOrganizationArea","idArea"=>$area_fobj->id,"return"=>["scr"=>"management","tab"=>"areas"]]),"fa-trash-o",api_text("table-td-undelete"),true,api_text("cOrganizationArea-confirm-undelete"));}
	else{$ob->addElement(api_url(["scr"=>"controller","act"=>"delete","obj"=>"cOrganizationArea","idArea"=>$area_fobj->id,"return"=>["scr"=>"management","tab"=>"areas"]]),"fa-trash",api_text("table-td-delete"),true,api_text("cOrganizationArea-confirm-delete"));}
	// make table row class
	$tr_class_array=array();
	if($area_fobj->id==$_REQUEST["idArea"]){$tr_class_array[]="currentrow";}
	if($area_fobj->deleted){$tr_class_array[]="deleted";}
	// make Organization row
	$areas_table->addRow(implode(" ",$tr_class_array));
	$areas_table->addRowField($area_fobj->getCompany()->name,"nowrap");
	$areas_table->addRowField($area_fobj->name,"nowrap");
	$areas_table->addRowField($area_fobj->description,"truncate-ellipsis");
	$areas_table->addRowField($ob->render(),"nowrap text-right");
}

// check for view action
if(ACTION=="area_view"){
	// get selected area
	$selected_area_obj=new cOrganizationArea($_REQUEST["idArea"]);
	// build description list
	$dl=new strDescriptionList("br","dl-horizontal");
	$dl->addElement(api_text("cOrganizationArea-property-fkCompany"),$selected_area_obj->getCompany()->name);
	$dl->addElement(api_text("cOrganizationArea-property-name"),api_tag("strong",$selected_area_obj->name));
	if($selected_area_obj->description){$dl->addElement(api_text("cOrganizationArea-property-description"),$selected_area_obj->description);}
	// build modal
	$modal=new strModal(api_text("management-areas-modal-title"),null,"management-areas-view");
	$modal->setBody($dl->render()."<hr>".api_logs_table($selected_area_obj->getLogs((!$_REQUEST["all_logs"]?10:null)))->render());
	// add modal to application
	$app->addModal($modal);
	// modal scripts
	$app->addScript("$(function(){\$('#modal_management-areas-view').modal();});");
}

// check for add or edit actions
if(in_array(ACTION,["area_add","area_edit"]) && api_checkAuthorization("organization-manage")){
	// get selected area
	$selected_area_obj=new cOrganizationArea($_REQUEST["idArea"]);
	// get form
	$form=$selected_area_obj->form_edit(["return"=>["scr"=>"management","tab"=>"areas"]]);
	// additional controls
	$form->addControl("button",api_text("form-fc-cancel"),"#",null,null,null,"data-dismiss='modal'");
	if($selected_area_obj->exists()){$form->addControl("button",api_text("form-fc-remove"),api_url(["scr"=>"controller","act"=>"remove","obj"=>"cOrganizationArea","idArea"=>$selected_area_obj->id,"return"=>["scr"=>"management","tab"=>"areas"]]),"btn-danger",api_text("cOrganizationArea-confirm-remove"));}
	// build modal
	$modal=new strModal(api_text("management-areas-modal-title"),null,"management-areas-edit");
	$modal->setBody($form->render(1));
	// add modal to application
	$app->addModal($modal);
	// modal scripts
	$app->addScript("$(function(){\$('#modal_management-areas-edit').modal({show:true,backdrop:'static',keyboard:false});});");
}
