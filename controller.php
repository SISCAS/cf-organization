<?php
/**
 * Organization - Controller
 *
 * @package Coordinator\Modules\Organization
 * @company Cogne Acciai Speciali s.p.a
 */

// debug
api_dump($_REQUEST,"_REQUEST");
// check if object controller function exists
if(function_exists($_REQUEST["obj"]."_controller")){
	// call object controller function
	call_user_func($_REQUEST["obj"]."_controller",$_REQUEST["act"]);
}else{
	api_alerts_add(api_text("alert_controllerObjectNotFound",[MODULE,$_REQUEST["obj"]."_controller"]),"danger");
	api_redirect("?mod=".MODULE);
}

/**
 * Company controller
 *
 * @param string $action Object action
 */
function cOrganizationCompany_controller($action){
	// check authorizations
	api_checkAuthorization("organization-manage","dashboard");
	// get object
	$company_obj=new cOrganizationCompany($_REQUEST["idCompany"]);
	api_dump($company_obj,"Company object");
	// check object
	if($action!="store" && !$company_obj->exists()){api_alerts_add(api_text("cOrganizationCompany-alert-exists"),"danger");api_redirect(api_url(["scr"=>"management","tab"=>"companies"]));}
	// execution
	try{
		switch($action){
			case "store":
				$company_obj->store($_REQUEST);
				api_alerts_add(api_text("cOrganizationCompany-alert-stored"),"success");
				break;
			case "delete":
				$company_obj->delete();
				api_alerts_add(api_text("cOrganizationCompany-alert-deleted"),"warning");
				break;
			case "undelete":
				$company_obj->undelete();
				api_alerts_add(api_text("cOrganizationCompany-alert-undeleted"),"warning");
				break;
			case "remove":
				$company_obj->remove();
				api_alerts_add(api_text("cOrganizationCompany-alert-removed"),"warning");
				break;
			default:
				throw new Exception("Company action \"".$action."\" was not defined..");
		}
		// redirect
		api_redirect(api_return_url(["scr"=>"management","tab"=>"companies","idCompany"=>$company_obj->id]));
	}catch(Exception $e){
		// dump, alert and redirect
		api_redirect_exception($e,api_url(["scr"=>"management","tab"=>"companies","idCompany"=>$company_obj->id]),"cOrganizationCompany-alert-error");
	}
}

/**
 * Division controller
 *
 * @param string $action Object action
 */
function cOrganizationDivision_controller($action){
	// check authorizations
	api_checkAuthorization("organization-manage","dashboard");
	// get object
	$division_obj=new cOrganizationDivision($_REQUEST["idDivision"]);
	api_dump($division_obj,"Division object");
	// check object
	if($action!="store" && !$division_obj->exists()){api_alerts_add(api_text("cOrganizationDivision-alert-exists"),"danger");api_redirect(api_url(["scr"=>"management","tab"=>"divisions"]));}
	// execution
	try{
		switch($action){
			case "store":
				$division_obj->store($_REQUEST);
				api_alerts_add(api_text("cOrganizationDivision-alert-stored"),"success");
				break;
			case "delete":
				$division_obj->delete();
				api_alerts_add(api_text("cOrganizationDivision-alert-deleted"),"warning");
				break;
			case "undelete":
				$division_obj->undelete();
				api_alerts_add(api_text("cOrganizationDivision-alert-undeleted"),"warning");
				break;
			case "remove":
				$division_obj->remove();
				api_alerts_add(api_text("cOrganizationDivision-alert-removed"),"warning");
				break;
			default:
				throw new Exception("Division action \"".$action."\" was not defined..");
		}
		// redirect
		api_redirect(api_return_url(["scr"=>"management","tab"=>"divisions","idDivision"=>$division_obj->id]));
	}catch(Exception $e){
		// dump, alert and redirect
		api_redirect_exception($e,api_url(["scr"=>"management","tab"=>"divisions","idDivision"=>$division_obj->id]),"cOrganizationDivision-alert-error");
	}
}

/**
 * Area controller
 *
 * @param string $action Object action
 */
function cOrganizationArea_controller($action){
	// check authorizations
	api_checkAuthorization("organization-manage","dashboard");
	// get object
	$area_obj=new cOrganizationArea($_REQUEST["idArea"]);
	api_dump($area_obj,"Area object");
	// check object
	if($action!="store" && !$area_obj->exists()){api_alerts_add(api_text("cOrganizationArea-alert-exists"),"danger");api_redirect(api_url(["scr"=>"management","tab"=>"areas"]));}
	// execution
	try{
		switch($action){
			case "store":
				$area_obj->store($_REQUEST);
				api_alerts_add(api_text("cOrganizationArea-alert-stored"),"success");
				break;
			case "delete":
				$area_obj->delete();
				api_alerts_add(api_text("cOrganizationArea-alert-deleted"),"warning");
				break;
			case "undelete":
				$area_obj->undelete();
				api_alerts_add(api_text("cOrganizationArea-alert-undeleted"),"warning");
				break;
			case "remove":
				$area_obj->remove();
				api_alerts_add(api_text("cOrganizationArea-alert-removed"),"warning");
				break;
			default:
				throw new Exception("Area action \"".$action."\" was not defined..");
		}
		// redirect
		api_redirect(api_return_url(["scr"=>"management","tab"=>"areas","idArea"=>$area_obj->id]));
	}catch(Exception $e){
		// dump, alert and redirect
		api_redirect_exception($e,api_url(["scr"=>"management","tab"=>"areas","idArea"=>$area_obj->id]),"cOrganizationArea-alert-error");
	}
}

/**
 * Department controller
 *
 * @param string $action Object action
 */
function cOrganizationDepartment_controller($action){
	// check authorizations
	api_checkAuthorization("organization-manage","dashboard");
	// get object
	$department_obj=new cOrganizationDepartment($_REQUEST["idDepartment"]);
	api_dump($department_obj,"Department object");
	// check object
	if($action!="store" && !$department_obj->exists()){api_alerts_add(api_text("cOrganizationDepartment-alert-exists"),"danger");api_redirect(api_url(["scr"=>"management","tab"=>"departments"]));}
	// execution
	try{
		switch($action){
			case "store":
				$department_obj->store($_REQUEST);
				api_alerts_add(api_text("cOrganizationDepartment-alert-stored"),"success");
				break;
			case "delete":
				$department_obj->delete();
				api_alerts_add(api_text("cOrganizationDepartment-alert-deleted"),"warning");
				break;
			case "undelete":
				$department_obj->undelete();
				api_alerts_add(api_text("cOrganizationDepartment-alert-undeleted"),"warning");
				break;
			case "remove":
				$department_obj->remove();
				api_alerts_add(api_text("cOrganizationDepartment-alert-removed"),"warning");
				break;
			default:
				throw new Exception("Department action \"".$action."\" was not defined..");
		}
		// redirect
		api_redirect(api_return_url(["scr"=>"management","tab"=>"departments","idDepartment"=>$department_obj->id]));
	}catch(Exception $e){
		// dump, alert and redirect
		api_redirect_exception($e,api_url(["scr"=>"management","tab"=>"departments","idDepartment"=>$department_obj->id]),"cOrganizationDepartment-alert-error");
	}
}
